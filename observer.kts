internal interface Observer {
    fun update(name: String, s: String)
}


internal class Celebrity(private val celebrityName: String  //name of the celebrity
) : Subject {
    private val followers: ArrayList<Observer>  //list of followers

    init {
        followers = ArrayList<Observer>()
    }


    @Override
    fun register(o: Observer) {
        followers.add(o)
        System.out.println("$o has started following $celebrityName")
    }


    @Override
    fun unregister(o: Observer) {
        followers.remove(o)
        System.out.println("$o has stopped following $celebrityName")
    }


    @Override
    fun notifyAllObservers(tweet: String) {
        for (follower in followers) {
            follower.update(celebrityName, tweet)
        }
        System.out.println()
    }


    fun tweet(tweet: String) {
        System.out.println("\n$celebrityName has tweeted :: $tweet\n")
        notifyAllObservers(tweet)
    }
}


internal class Follower(private val followerName: String) : Observer {

    @Override
    override fun update(celebrityName: String, tweet: String) {
        System.out.println("$followerName has received $celebrityName's tweet    :: $tweet")
    }

    @Override
    fun toString(): String {
        return followerName
    }

}